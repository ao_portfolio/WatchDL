## Project description
In dit project is er gewerkt aan een wearOS app (developed in android studio met Kotlin) dat een dashboard is voor De Lijn, 
dit is te vergelijken met de informatie borden die bij enkele haltes aanwezig zijn die aangeven: "Bus X: 5 min".
Bij deze app zijn ook extra functionaliteiten toegevoegd zoals:
    -Companion app op de gsm: Omdat het scherm van het horloge klein is maakt dit het moeilijk om zaken in te stellen. 
    Hierbij is er dus gekozen om dit via de gsm te doen en deze via de ChannelClient door te sturen naar het horloge 
    (van deze instellingen word er ook een backup gemaakt in firebase). Deze instellingen zijn onderandere de haltes om te weergeven
    en een lijst van welke bussen per halte om te weergeven.
    -automatisch opstarten app: De app op het horloge heeft ook een background service die om de X minuten checkt 
    (X is afhankelijk van de afstand tot halte) ofdat deze dicht genoeg is bij een halte. Indien dit zo is start deze het dashboard
    op de pagina van de desbetreffende halte.

## Extra info
Om de watchapp te kunnen gebruiken is de companion app op de gsm noodzakelijk om haltes te kunnen toevoegen.

Voor de watchapp moet de setting: "Display over other apps" aanstaan zodat de background thread die je locatie checkt de app kan openen.
Deze setting kan je vinden bij: Settings > Apps & Notifications > App info > WatchDL > Advanced
